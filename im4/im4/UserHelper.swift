//
//  UserHelper.swift
//  im4
//
//  Created by Juan Martinez on 22/3/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation

class UserDataHelper: GenericProtocol {
    
    static let ENDPOINT = "/user"
    
    typealias T = User
    
    static func insert(item: T) -> Int {
        // post retrun ID or -1
        return -1
        
    }
    
    static func delete (item: T) -> Void {
        // delete
    }
    
    static func find(id: Int) -> T? {
        // find by id
        let ret:T = User()
        return ret
    }
    
    static func findAll() -> [T]? {
        let retArray = [T]()
        // findAll
        return retArray
    }
}
