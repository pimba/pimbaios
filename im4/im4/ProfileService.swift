//
//  ProfileService.swift
//  im4
//
//  Created by Juan Martinez on 22/3/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class ProfileService : DefultRestConfig{
    
    override init() {
        super.init()
        self.host = "http://10.59.25.236:9290"
        self.endPoint = "mlevel/service/profile"
        self.headers = [
            "Authorization": "b77ce073-bcc9-4617-a5ad-96761dfdfa25",
            "Content-Type": "application/json"
        ]
    }
    
    func getProfile(completionHandler: (Profile?, NSError?) -> ()) {
        
        Alamofire.request(.GET, "\(host)/\(endPoint)", headers: headers)
            .validate(statusCode : 200..<300 )
            .responseObject() { (response: Response<Profile, NSError>) in
                print(response.timeline)
                switch response.result {
                case .Success(let value):
                    completionHandler(value , nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    /*
     func makeCall(section: String, completionHandler: (Profile?, NSError?) -> ()) {
     
     Alamofire.request(.GET, "\(host)/\(endPoint)", parameters: params)
     //.authenticate(user: consumerKey, password: consumerSecret)
     .responseJSON { response in
     switch response.result {
     case .Success(let value):
     completionHandler(value as? Profile, nil)
     case .Failure(let error):
     completionHandler(nil, error)
     }
     }
     }*/
    
    
}