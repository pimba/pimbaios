//
//  WallViewController.swift
//  im4
//
//  Created by Juan Martinez on 3/4/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation

class WallViewController: UITableViewController {
    
    @IBOutlet var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Wall view controller")
    }
    
    
}
