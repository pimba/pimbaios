//
//  DefaultRestConfig.swift
//  im4
//
//  Created by Juan Martinez on 22/3/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation

class DefultRestConfig {
    
    
    var host:String!
    var endPoint = "/melevel/hello"
    var params = [String:String]()
    var headers = [String:String]()
    
    var consumerKey:String!
    var consumerSecret:String!
    var authorization:String!
    var uuid:String!
    
    var returnData = [:]
    
    init(){
        self.consumerKey = "PimaKey"
        self.consumerSecret = "PimbaPass"
        self.host = ""
        self.authorization = ""
        self.uuid = ""
    }
}