//
//  InitialViewController.swift
//  im4
//
//  Created by Juan Martinez on 17/3/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import UIKit


class InitialViewController: UIViewController {
    

    @IBOutlet weak var logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*if let checkedUrl = NSURL(string: "https://tanconectados.files.wordpress.com/2009/02/logo.png?w=500") {
            logo.contentMode = .ScaleAspectFit
            self.downloadImage(checkedUrl)
            self.logo.sizeToFit()
            
        }*/
        
        print("Initial View Controller")
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    @IBAction func bLogin(sender: AnyObject) {
    
    }
    
    
    func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }
    
    func downloadImage(url: NSURL){
        print("Download Started")
        print("lastPathComponent: " + (url.lastPathComponent ?? ""))
        getDataFromUrl(url) { (data, response, error)  in
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                guard let data = data where error == nil else { return }
                print(response?.suggestedFilename ?? "")
                print("Download Finished")
                self.logo.image = UIImage(data: data)
            }
        }
    }

    
}

