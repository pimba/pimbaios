//
//  GenericaDAO.swift
//  im4
//
//  Created by Juan Martinez on 22/3/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation

protocol GenericProtocol {
    associatedtype T
    
    static func insert(item: T) -> Int
    static func delete(item: T) -> Void
    static func findAll() -> [T]?
}