//
//  ImageUtils.swift
//  im4
//
//  Created by Juan Martinez on 2/4/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation

class ImageUtils {
    
    
    func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }
    
    func downloadImage(completionHandler: (NSData?) -> (), url: NSURL){
        print("Download Started")
        print("lastPathComponent: " + (url.lastPathComponent ?? ""))
        getDataFromUrl(url) { (data, response, error)  in
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                print(response?.suggestedFilename ?? "")
                print("Download Finished")
                //imageView.image = UIImage(data: data)
            }
        }
    }
    
}