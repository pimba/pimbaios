//
//  User.swift
//  im4
//
//  Created by Juan Martinez on 17/3/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation


class User {
    var name  : String = ""
    var token : String = ""
    var email : String = ""
}