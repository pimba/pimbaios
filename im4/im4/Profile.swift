//
//  Profile.swift
//  im4
//
//  Created by Juan Martinez on 22/3/16.
//  Copyright © 2016 Juan Martinez. All rights reserved.
//

import Foundation
import ObjectMapper

class Profile: Mappable {
    var welcome_message: String = ""
    var sex: String?
    var discovery : Bool?
    var min_age : Int?
    var max_age : Int?
    var discovery_sex : String?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        welcome_message <- map["welcome_message"]
        sex <- map["sex"]
        discovery <- map["discover"]
        min_age <- map["min_age"]
        max_age <- map["max_age"]
        discovery_sex <- map["discover_sex"]
    }
    
    func toString() -> String{
        return " Profile:  M: \(welcome_message) S:\(sex), D: \(discovery) \n MinA: \(min_age), MaxA: \(max_age), DS: \(discovery_sex)"
    }
}
